import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpProvider } from "../../providers/http/http";
import { ConstantProvider } from '../../providers/constant/constant';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;
  loading: Loading;
  seen: boolean = true;
  pwInputType = 'password';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public httpProvider: HttpProvider,
    public constantProvider: ConstantProvider,) {
      this.buildForm();
  }

  buildForm() {
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goRegister() {
    this.navCtrl.push('register');
  }

  seePassword() {
    this.seen = !this.seen;
    if (this.seen) {
      this.pwInputType = "password"
    } else {
      this.pwInputType = "text"
    }
  } 

  forgotPassword() {
    alert("forgot password");
  }

  doFacebookLogin() {
    alert("do facebook login");
  }

  doLogin({ value, valid }) {
    if (valid) {
      this.loading = this.constantProvider.loading();
      let parameter: any = {};
      parameter = value;
      console.log("parameter when login ==>", parameter);
      this.httpProvider.post("auth/login", parameter).subscribe(data => {
        let res = data.json();
        if (res.error == true) {
          this.loading.dismiss().then(() => {
            this.constantProvider.basicAlert("Error!", "Invalid Login! Username and password are wrong.");
          })
        }else{
          this.loading.dismiss().then(() => { 
            this.constantProvider.toast("Successfully Login!");
            localStorage.setItem("id",res.data.id);
            let isBooking = res.isBooking;
            if(isBooking == 1) {
              this.navCtrl.push('confirm');
            } else {
              this.navCtrl.push('booking');
            }            
          });
        }
      })
    }
  }
}
