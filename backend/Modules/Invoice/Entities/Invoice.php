<?php

namespace Modules\Invoice\Entities;

use App\ARmodel as Model;

class Invoice extends Model
{
    protected $fillable = [];
    protected $table = "invoice";

    public function invoice_items(){
    	return $this->hasMany("Modules\Invoice\Entities\InvoiceItem","invoice_id");
    }

    public function invoice_credits(){
    	return $this->hasMany("Modules\Credit\Entities\Credit","invoice_id");
    }

}
