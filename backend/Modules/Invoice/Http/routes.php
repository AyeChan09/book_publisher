<?php

Route::group(['middleware' => [], 'prefix' => 'invoice', 'namespace' => 'Modules\Invoice\Http\Controllers'], function()
{
    Route::get('/', 'InvoiceController@index');
    Route::resource('invoice', 'InvoiceController');
    Route::get('get_voucher/{id}', 'InvoiceController@getVoucher');
    Route::get('get_balance', 'InvoiceController@getBalance');
});
