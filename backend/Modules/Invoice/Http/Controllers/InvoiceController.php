<?php

namespace Modules\Invoice\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Schema;

use Modules\Invoice\Entities\Invoice;
use Modules\Invoice\Entities\InvoiceItem;
use Modules\Book\Entities\Book;

class InvoiceController extends Controller
{
    private $entity;
    public function __construct(){
        $this->entity = new Invoice;
    }
    
    public function index()
    {
        return view('invoice::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('invoice::create');
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $data = $this->entity;
        foreach($input as $key=>$value){
            if(Schema::hasColumn($data->getTable(), $key)){
                $data->$key=$value;
            }
        }
        $data->status = 'none';
        $data->balance = $input['total_amount'];
        $data->save();
        foreach($input['items'] as $book) {
            $invoice_item = new InvoiceItem;
            foreach($book as $key=>$value){
                if(Schema::hasColumn($invoice_item->getTable(), $key)){ 
                    $invoice_item->$key = $value;
                }
            } 
            $invoice_item->invoice_id = $data->id;
            $invoice_item->save();

            $balance = Book::where('id', $book['book_id'])->first()->balance;
            $balance -= $book['qty'];
            $book_info = Book::where('id', $book['book_id'])->update(['balance' => $balance]);

        }
        if(isset($data)){
            $return['data'] = $data;
            $return['error'] = false;
            $return['msg'] = 'success';
        }
        else{
            $return['data'] = $data;
            $return['error'] = true;
            $return['msg'] = 'fail';
        }
        return $return;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('invoice::show');
    }

    public function getVoucher(Request $request, $id) 
    {
        // $input = $request->input();
        $data = $this->entity::select('voucher_no')->where('status', '<>', 'receive')->where('book_shop_id', $id)->get();
        return $data;
    }

    public function getBalance(Request $request)
    {
        $input = $request->input();
        $data = $this->entity::where('status', '<>', 'receive')->where('voucher_no', $input['voucher_no'])->where('book_shop_id', $input['book_shop_id'])->first()->balance;
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('invoice::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
