<?php

namespace Modules\Generic\Entities;

use Illuminate\Database\Eloquent\Model;

class userSavedFilter extends Model
{
    protected $fillable = [];
    protected $table = "user_saved_filter";   
    
}
