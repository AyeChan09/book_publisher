<?php

namespace Modules\Generic\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Generic\Entities\UserSavedFilter;

class SavedFilterController extends Controller
{
    private $entity;
    public function __construct(){
        $this->entity = new UserSavedFilter;
    }
    public function index(Request $request)
    {
        $input = $request->input();
        $user_id = session("user_id") != null ? session("user_id") : 1;        
        $filter_type = $input['filter_type'];
        $data = $this->entity->where("user_id",$user_id)->where("filter_type",$filter_type)->get();
        $return['data'] = $data;
        return $return;
    }
    public function store(Request $request)
    {   
        $input = $request->input();
        $user_id = session("user_id") != null ? session("user_id") : 1;        
        $filter_type = $input['filter_type'];
        $filter_name = $input['filter_name'];
        $filters = $input['filters'];
        $data = $this->entity->find($input['filter_id']);
        if(!isset($data->id)){
            $data = new $this->entity;
        }
        $data->user_id = $user_id;
        $data->filter_type = $filter_type;
        $data->filter_name = $filter_name;
        $data->filters = $filters;
        $data->save();
        return $input;
    }


    public function destroy($id)
    {
        $this->entity::find($id)->delete();
        $return['error'] = false;
        $return['msg'] = "ok";
        return $return;
    }
}
