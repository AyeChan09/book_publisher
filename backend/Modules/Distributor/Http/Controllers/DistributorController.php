<?php

namespace Modules\Distributor\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Schema;

use Modules\Distributor\Entities\Distributor;

class DistributorController extends Controller
{
    private $entity;
    public function __construct(){
        $this->entity = new Distributor;
    }
    
    public function index(Request $request)
    {
        $input=$request->input();
        $page=$input['page'];
        $limit=$input['limit'];
        $search=json_decode($input['search'],true);
        $order = json_decode($input['order'],true);         
        $data = $this->entity::offset(($page-1)*$limit)->limit($limit); 
        
        if(isset($search['name']) && $search['name'] != ""){
            $data->where("name", 'like',"%".$search['name']."%");
        }
        if(isset($search['address']) && $search['address'] != ""){
            $data->where("address", 'like',"%".$search['address']."%");
        }
        if(isset($search['gender']) && $search['gender'] != ""){
            $data->where("gender", 'like',"%".$search['gender']."%");
        }
        if(isset($order['by']) && $order['by'] != ""){
            $order_dir = $order['dir'] == "" ? "desc" : $order['dir'];
            $data->orderBy($order['by'], $order_dir);
        }         
        $totalData = $data->count("id");
        $data=$data->get();
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }

    public function login(Request $request){
        $input = $request->input();
        $data = $this->entity::where("name",$input['name'])->where("password",$input['password'])->first();            
        if(isset($data)){
            $return['data'] = $data;
            $return['error'] = false;
            $return['msg'] = 'success';
        }
        else{
            $return['error'] = true;
            $return['msg'] = 'fail';
        }
        return $return;
    }

    public function changePassword(Request $request,$id)
    {
        $input = $request->input();
        $data = $this->entity->find($id);
        $data->password = $input['newPassword'];
        $data->save();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('distributor::create');
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $data = $this->entity;
        foreach($input['parameter'] as $key=>$value){
            if(Schema::hasColumn($data->getTable(), $key)){
                $data->$key=$value;
            }
        }
        $data;
        $data->save();
        if(isset($data)){
            $return['data'] = $data;
            $return['error'] = false;
            $return['msg'] = 'success';
        }
        else{
            $return['data'] = $data;
            $return['error'] = true;
            $return['msg'] = 'fail';
        }
        return $return;
    }

    public function show(Request $request, $id)
    {
        $return = array();
        $data = $this->entity->where("id", $id)->first();
        if(isset($data)){
            $return['data'] = $data;
            $return['error'] = false;
            $return['msg'] = 'success';
        }
        else{
            $return['data'] = $data;
            $return['error'] = true;
            $return['msg'] = 'fail';
        }            
        return $return;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('distributor::edit');
    }

    public function update(Request $request, $id)
    {
        $input = $request->input();
        $post = $input['parameter'];
        $data = $this->entity->find($id);
        foreach($post as $key=>$value) {
            if(Schema::hasColumn($data->getTable(), $key)) {
                $data->$key = $value;
            }
        }
        $data->save();
        return $data;
    }

    public function destroy($id)
    {
        $this->entity::find($id)->delete();
        $return['error'] = false;
        $return['msg'] = "ok";
        return $return;
    }
}
