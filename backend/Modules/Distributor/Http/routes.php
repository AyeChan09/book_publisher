<?php

Route::group(['middleware' => [], 'prefix' => 'distributor', 'namespace' => 'Modules\Distributor\Http\Controllers'], function()
{
    Route::get('/', 'DistributorController@index');
    Route::resource('distributor', 'DistributorController');
    Route::post("call_helper","HelperController@call");   
    Route::post('login', 'DistributorController@login');
    Route::get('change_password/{id}','ParentController@changePassword');

});
