<?php

namespace Modules\Distributor\Entities;

use App\ARmodel as Model;

class Distributor extends Model
{
    protected $fillable = [];
    protected $table = "distributor";
}
