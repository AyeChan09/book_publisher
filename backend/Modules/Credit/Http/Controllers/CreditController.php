<?php

namespace Modules\Credit\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Schema;

use Modules\Invoice\Entities\Invoice;
use Modules\Credit\Entities\Credit;

class CreditController extends Controller
{
    private $entity;
    public function __construct(){
        $this->entity = new Credit;
    }
    
    public function index()
    {
        return view('credit::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('credit::create');
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $invoice = Invoice::where('voucher_no', $input['voucher_no'])->where('book_shop_id', $input['book_shop_id'])->first();
        $data = $this->entity;
        foreach($input as $key=>$value){
            if(Schema::hasColumn($data->getTable(), $key)){
                $data->$key=$value;
            }
        }
        $data->invoice_id = $invoice['id'];
        $data->total = $input['payment_amount'];
        if($input['balance'] != 0) 
            $data->status = 'half';
        else 
            $data->status = 'paid';
        $data->save();

        // $invoice = Invoice::where('voucher_no', $input['voucher_no'])->first();
        if($data->status == 'half') {
            $invoice->status = 'half-receive';
        } else {
            $invoice->status = 'receive';
        }
        $invoice->balance = $input['balance'];
        $invoice->save();
        if(isset($data)){
            $return['data'] = $data;    
            $return['error'] = false;
            $return['msg'] = 'success';
        }
        else{
            $return['data'] = $data;
            $return['error'] = true;
            $return['msg'] = 'fail';
        }
        return $return;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('credit::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('credit::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
