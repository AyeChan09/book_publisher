<?php

Route::group(['middleware' => [], 'prefix' => 'credit', 'namespace' => 'Modules\Credit\Http\Controllers'], function()
{
    Route::get('/', 'CreditController@index');
    Route::resource('credit', 'CreditController');
});
