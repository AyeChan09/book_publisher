<?php

Route::group(['middleware' => [], 'prefix' => 'auth', 'namespace' => 'Modules\Auth\Http\Controllers'], function()
{
    Route::post('/register', 'AuthController@register');
    Route::post('/login', 'AuthController@login');
    Route::post('/logout','AuthController@logout');
    Route::post('/session_check', 'AuthController@sessionCheck');
});
