<?php

Route::group(['middleware' => [], 'prefix' => 'book', 'namespace' => 'Modules\Book\Http\Controllers'], function()
{
    Route::get('/', 'BookController@index');
    Route::resource('/book', 'BookController');
    Route::post("call_helper","HelperController@call");   
});
