<?php

namespace Modules\Report\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

use Modules\Invoice\Entities\Invoice;
use Modules\Book\Entities\Book;
use Modules\Credit\Entities\Credit;

class ReportController extends Controller
{
    public function index()
    {
        return view('report::index');
    }

    public function sale(Request $request)
    {
        $input = $request->input();
        $distributor = $input['distributor'];
        $bookShop = $input['book_shop'];
        // $data = Invoice::with('invoice_credits')
        //         ->get();
        
        // select invoice.voucher_no, DATE(invoice.created_at) as date, invoice.total_amount as total, DATE(credit.created_at) as cash_date, credit.total as amount, (invoice.total_amount - credit.total) as balance 
        // from invoice 
        // left join credit on invoice.id = credit.invoice_id

        $data = Invoice::leftJoin('credit', 'invoice.id', 'credit.invoice_id')
                ->select('invoice.voucher_no', DB::raw('DATE(invoice.created_at) as date'), 'invoice.total_amount as total', DB::raw('DATE(credit.created_at) as cash_date'), 'credit.total as amount', DB::raw('invoice.total_amount - credit.total as balance '))
                ->where('invoice.distributor_id', $distributor)
                ->where('invoice.book_shop_id', $bookShop)
                ->get();
        return $data;
    }

    public function stock(Request $request)
    {
        $data = Book::select('name', 'balance', DB::raw('qty - balance as soldout'))
                ->get();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('report::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('report::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('report::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
