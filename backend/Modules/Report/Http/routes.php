<?php

Route::group(['middleware' => [], 'prefix' => 'report', 'namespace' => 'Modules\Report\Http\Controllers'], function()
{
    Route::get('/', 'ReportController@index');
    Route::get('sale', 'ReportController@sale');
    Route::get('stock', 'ReportController@stock');
});
