<?php

Route::group(['middleware' => [], 'prefix' => 'book_shop', 'namespace' => 'Modules\BookShop\Http\Controllers'], function()
{
    Route::get('/', 'BookShopController@index');
    Route::resource('/book_shop', 'BookShopController');
    Route::post("call_helper","HelperController@call");   
});
