<?php

namespace Modules\BookShop\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class HelperController extends Controller
{
    public function call(Request $request){
        $function = $request->input("function");
        return $this->$function($request);
    }

    public function book_shop(Request $request){
        $return = DB::table("book_shop")->get();
        return $return;
    }
}