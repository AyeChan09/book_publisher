import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './shared/auth/auth-guard.service';
import { SynapseComponent } from "./synapse/synapse.component";
import { LoginComponent } from "./synapse/authentication/login/login.component";
const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'synapse/dashboard',
    pathMatch: 'full',
  },
  { path: 'synapse', loadChildren: 'app/synapse/synapse.module#SynapseModule'} 
  
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes,{useHash:true})],
  exports: [RouterModule]  
})

export class AppRoutingModule {
}