
function makeAppConfig() {
    const date = new Date();
    const year = date.getFullYear();

    const AppConfig = {
        brand: 'ARzap',
        user: 'Mike',
        year,
        baseUrl: 'http://localhost:8000',
        // baseUrl: 'http://localhost/erp/synapse2/logic/public',
        recordPerPage : [50,100,1000]
    };

    return AppConfig;
}

export const APPCONFIG = makeAppConfig();
