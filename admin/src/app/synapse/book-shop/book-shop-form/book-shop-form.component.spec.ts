import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookShopFormComponent } from './book-shop-form.component';

describe('BookShopFormComponent', () => {
  let component: BookShopFormComponent;
  let fixture: ComponentFixture<BookShopFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookShopFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookShopFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
