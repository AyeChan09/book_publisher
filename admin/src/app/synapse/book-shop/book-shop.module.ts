import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "../../shared/shared.module";
import { BookShopFormComponent } from './book-shop-form/book-shop-form.component';
import { BookShopListComponent } from './book-shop-list/book-shop-list.component';

export const routes = [
  { path: 'book_shop_list', component: BookShopListComponent },
  { path: 'book_shop_form', component: BookShopFormComponent },
  { path: 'book_shop_form/:id', component: BookShopFormComponent },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BookShopFormComponent, BookShopListComponent]
})
export class BookShopModule { }
