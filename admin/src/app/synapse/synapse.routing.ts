import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { DashboardComponent } from './dashboard/dashboard.component';

import { SynapseComponent } from './synapse.component';
import { LoginComponent } from "./authentication/login/login.component";

export const routes: Routes = [
    {
        path: '', 
        component: SynapseComponent,
        children:[
            { path: 'dashboard',component:DashboardComponent},            
            { path: 'book', loadChildren: 'app/synapse/book/book.module#BookModule', data: { breadcrumb: 'Book' } },
            { path: 'book_shop', loadChildren: 'app/synapse/book-shop/book-shop.module#BookShopModule', data: { breadcrumb: 'BookShop' } },
            { path: 'distributor', loadChildren: 'app/synapse/distributor/distributor.module#DistributorModule', data: { breadcrumb: 'Distributor' } },
            { path: 'report', loadChildren: 'app/synapse/report/report.module#ReportModule', data: { breadcrumb: 'Report' } },
        ]
    },
    { path: 'login', component:LoginComponent, data: { breadcrumb: 'Login' } },
    
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);