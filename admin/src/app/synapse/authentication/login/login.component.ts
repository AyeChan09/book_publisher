import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { HttpService } from "../../../shared/http.service";

@Component({
  selector: 'az-login',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],  
  providers:[HttpService]
})
export class LoginComponent {
    public form:FormGroup;
    public username:AbstractControl;
    public password:AbstractControl;
    public errFlag:boolean = false;

    constructor(
        public router:Router,
        public fb:FormBuilder,
        public HttpService : HttpService,
    ) {
        this.form = fb.group({
            'username': ['root', Validators.compose([Validators.required])],
            'password': ['root', Validators.compose([Validators.required])]
        });

        this.username = this.form.controls['username'];
        this.password = this.form.controls['password'];
    }

    public onSubmit(values:Object):void {
        if(this.form.valid){
            let rt : any = this.HttpService.post("/auth/login",this.form.getRawValue());
            rt.subscribe((data) =>{
                let res = data.json();
                let message = "failed";
                let action = "close";
                if(!res.error){
                    message = "successful";
                    // localStorage.setItem('token', res.token);
                    // localStorage.setItem('admin_id', res.admin_id);
                    // localStorage.setItem('menu_arzap_hotel', JSON.stringify(res.sidebar.menu));
                    // localStorage.setItem('access_arzap_hotel', JSON.stringify(res.sidebar.access));
                    localStorage.setItem("ibeautyUserToken",res.token);
                    localStorage.setItem("accessMenus",JSON.stringify(res.sidebar));
                    
                    this.router.navigate(["/"]);                    
                }else{
                    this.errFlag =  true;
                }
            });
        }
    }
}
export function emailValidator(control: FormControl): {[key: string]: any} {
    var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;    
    if (control.value && !emailRegexp.test(control.value)) {
        return {invalidEmail: true};
    }
}
