import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaleReportComponent } from './sale-report/sale-report.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { StockReportComponent } from './stock-report/stock-report.component';

export const routes = [
  { path: 'sale', component: SaleReportComponent },
  { path: 'stock', component: StockReportComponent },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SaleReportComponent, StockReportComponent]
})
export class ReportModule { }
