import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../shared/http.service';
import * as moment from 'moment';
import swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sale-report',
  templateUrl: './sale-report.component.html',
  styleUrls: ['./sale-report.component.scss']
})
export class SaleReportComponent implements OnInit {
  distributors: any = [];
  bookShops: any = [];
  recordData: any = [];
  search: any = {};

  constructor(
    private httpService: HttpService,
    public translate: TranslateService,
  ) { }

  ngOnInit() {
    this.httpService.post('/distributor/call_helper', { function: 'distributor' }).subscribe((data) => {
      let res = data.json();
      this.distributors = res;
    });

    this.httpService.post('/book_shop/call_helper', { function: 'book_shop' }).subscribe((data) => {
      let res = data.json();
      this.bookShops = res;
    });
  }

  getData() {
    let param: any;
    console.log('search=>', this.search);
    param = this.search;
    this.recordData = [];
    // this.isData = false;
    this.httpService.get("/report/sale", param).subscribe((data) => {
      let res = data.json();
      this.recordData = res;
      console.log('data=>', this.recordData);
      if(this.recordData.length > 0) {
        // this.isData = true;
      } else {

        let text: any = this.translate.get("No Record to show");
        swal({
          text: text.value,
          type: 'info',
          confirmButtonColor: '#0CC27E',
          confirmButtonText: 'Ok',
        }).then(function (isConfirm) {
          if (isConfirm) {
            swal.close();
          }
        },function(dismiss) {
          swal.close();
        });
      }
      
    });
  }

}
