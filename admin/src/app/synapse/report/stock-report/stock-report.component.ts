import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../shared/http.service';
import { TranslateService } from '@ngx-translate/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-stock-report',
  templateUrl: './stock-report.component.html',
  styleUrls: ['./stock-report.component.scss']
})
export class StockReportComponent implements OnInit {
  recordData: any = [];

  constructor(
    private httpService: HttpService,
    public translate: TranslateService,
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.recordData = [];
    // this.isData = false;
    this.httpService.get("/report/stock").subscribe((data) => {
      let res = data.json();
      this.recordData = res;
      console.log('data=>', this.recordData);
      if(this.recordData.length > 0) {
        // this.isData = true;
      } else {

        let text: any = this.translate.get("No Record to show");
        swal({
          text: text.value,
          type: 'info',
          confirmButtonColor: '#0CC27E',
          confirmButtonText: 'Ok',
        }).then(function (isConfirm) {
          if (isConfirm) {
            swal.close();
          }
        },function(dismiss) {
          swal.close();
        });
      }
      
    });
  }

}
