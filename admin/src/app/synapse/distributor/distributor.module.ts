import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "../../shared/shared.module";
import { DistributorListComponent } from './distributor-list/distributor-list.component';
import { DistributorFormComponent } from './distributor-form/distributor-form.component';

export const routes = [
  { path: 'distributor_list', component: DistributorListComponent },
  { path: 'distributor_form', component: DistributorFormComponent },
  { path: 'distributor_form/:id', component: DistributorFormComponent },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DistributorListComponent, DistributorFormComponent]
})
export class DistributorModule { }
