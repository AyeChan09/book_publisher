import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { APPCONFIG } from '../../../config';
import { HttpService } from '../../../shared/http.service';
import { TranslateService } from '@ngx-translate/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-distributor-form',
  templateUrl: './distributor-form.component.html',
  styleUrls: ['./distributor-form.component.scss']
})
export class DistributorFormComponent implements OnInit {
  recordForm: any = FormGroup;
  data: any = [];
  action: string = "create";
  private sub: any;
  id: number = 0;
  public AppConfig: any;

  constructor(
    public activatedRoute: ActivatedRoute,
    public httpService: HttpService,
    private formBuilder: FormBuilder,
    private location: Location,
    public translate: TranslateService,
  ) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty("id")) {
        this.id = params['id'];
      }
    });
    this.buildForm();
    this.AppConfig = APPCONFIG;
    if (this.id != 0) {
      this.httpService.get("/distributor/distributor/" + this.id, this.data).subscribe((data) => {
        let res = data.json();
        this.data = res.data;
        this.recordForm.patchValue(res.data);
      });
    }
  }

  buildForm() {
    this.recordForm = this.formBuilder.group({
      name: ['', Validators.required],
      password: ['', Validators.required],
      phone: [''],
      nrc: ['', Validators.compose([Validators.required,Validators.pattern(/^([\d]{1,2})\/([\w]{3}|[\w]{6})\(?::NAING|N\)([\d]{6})$/)])],
      address: [''],
      gender: [''],
    })
  }

  doBack() {
    this.location.back();
  }

  onSubmit({ value, valid }) {
    if (valid == false) {
      this.validateforms();
    } else {
      let parameter: any = {};
      parameter = value;
      if (this.id == 0) {
        let rt: any = this.httpService.post("/distributor/distributor", { 'parameter': parameter });
        rt.subscribe((data) => {
          let res = data.json();
          
          let message = "failed";
          let action = "close";
          if (!res.error) {
            message = "successful";
            this.doBack();
          } else {
          }
        });
      }
      else {
        console.log(parameter);
        let rt: any = this.httpService.put("/distributor/distributor/" + this.id, { 'parameter': parameter });
        rt.subscribe((data) => {
          let res = data.json();
          let message = "failed";
          let action = "close";
          if (!res.error) {
            message = "successful";
            this.doBack();
          } else {
          }
        });
      }
    }
  }

  validateforms() {
    Object.keys(this.recordForm.controls).forEach(field => {
      const control = this.recordForm.get(field);
      control.markAsTouched({ onlySelf: true });
      console.log(control);
    });
    let title: any = this.translate.get("Invalid!");
    let text: any = this.translate.get("Fields Are Required");
    swal({
      title: title.value,
      text: text.value,
      type: 'info',
      confirmButtonColor: '#0CC27E',
      confirmButtonText: 'Ok',
    }).then(function (isConfirm) {
      if (isConfirm) {
        swal.close();
      }
    }, function (dismiss) {
      swal.close();
    });
  }
}
