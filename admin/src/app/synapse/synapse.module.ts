import { NgModule } from '@angular/core';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from "../shared/shared.module";
import { ToastModule, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommonModule } from '@angular/common';
import { SynapseComponent } from './synapse.component';
// import { SearchComponent } from './search/search.component';
import { routing } from './synapse.routing';
import { CustomOption } from "../shared/toastr/custom-option";
import { AuthService } from '../shared/auth/auth.service';
import { AuthGuard } from '../shared/auth/auth-guard.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { KeypadComponent } from "../shared/component/keypad/keypad.component";
import { CustomTranslateLoader } from "../shared/custom-translate-loader";
import { LoginComponent } from "./authentication/login/login.component";
import { Http } from '@angular/http';

export function createTranslateLoader(http: Http) {
  return new CustomTranslateLoader(http);
}

@NgModule({
  imports: [
    // BrowserAnimationsModule,
    ToastModule.forRoot(),
    HttpClientModule,
    CommonModule,
    HttpModule,
    routing,
    SharedModule,
    NgbModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBr5_picK8YJK7fFR2CPzTVMj6GG1TtRGo'
    })
  ],
  declarations: [
    SynapseComponent,
    DashboardComponent,
    LoginComponent,
  ],
  entryComponents:[
    KeypadComponent,
  ],
  providers: [
    //Toastr and auth providers
    { provide: ToastOptions, useClass: CustomOption },
    AuthService,
    AuthGuard
  ],
})
export class SynapseModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}