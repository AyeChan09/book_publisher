import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../../../shared/http.service';
import { TranslateService } from '@ngx-translate/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {
  private allow_add = false;
  private allow_edit = false;
  private allow_delete = false;
  
  data: any = [];
  selectedRecord: any = {};
  currentPage: number = 1;
  totalTableData: number = 0;
  pageLimitOptions: any = [10 , 20, 30];
  pageLimit: number = this.pageLimitOptions[0];
  order: any = { "by": "created_at", "dir": "desc" };

  search: any = {};
  pagination: any = {};
  currentFunction: string = "book";
  currentRoute: string = this.router.url;
  savedFilter: any = [];
  activeFilterId: number = 0;
  activeFilterName: string = "";
  tableHeader: any = [];

  public selections: any = {};

  constructor(
    public router: Router,
    public httpService: HttpService,
    public translate: TranslateService
  ) { 
    this.httpService.post("/generic/get_access", { route: this.currentRoute }).subscribe((data) => {
      let res = data.json();
      let array: Array<any> = res.data
      array.forEach(item => {
        let allow = true;
        if (item.access_type === "none") {
          allow = false;
        }
        this[item.name] = allow;
      });
    });
    this.httpService.post("/generic/get_header", { list_name: this.currentFunction }).subscribe((data) => {
      let res = data.json();
      this.tableHeader = res.data;
    });
    this.loadData();
  }

  ngOnInit() {
    this.getPage(1, 1);
    this.getSavedFilter();
  }
  clearFilter() {
    this.search = {};
    this.getPage(1, 1);
    this.activeFilterId = 0;
    this.activeFilterName = "";
  }
  getPage(page: number, filter = 0) {
    this.currentPage = page;
    this.data = this.loadData().do(res => {
      let ret = res.json();
      if (filter == 1) {
        this.totalTableData = ret.total;
      }
    })
      .map(res => res.json().data);

    return 1; //return for callback purpose, if not it'll be ignored by subsribe in confirm
  }
  loadData() {
    let param: any = {};
    param.limit = this.pageLimit;
    param.page = this.currentPage;
    param.search = this.search;
    param.order = this.order;
    console.log('parametr=>', param);
    return this.httpService.get("/book/book", param);
  }
  selectData(record) {
    let main = this;
    this.selectedRecord = record;
    let title: any = this.translate.get("alert");
    let text: any = this.translate.get("delete_data_?");
    swal({
      title: title.value,
      text: text.value,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Confirm',
      cancelButtonText: 'Cancel'
    }).then(function (isConfirm) {
      if (isConfirm) {
        main.doDelete();
      }
    },function(dismiss) {
      swal.close();
    });
  }
  doEdit(id) {
    this.router.navigate(['synapse/book/book_form/' + id]);
  }
  doAdd() {
    this.router.navigate(['synapse/book/book_form']);
  }
  doDelete() {
    this.httpService.delete("/book/book/" + this.selectedRecord.id).subscribe((data) => {
      let res = data.json();
      let message = "failed";
      let action = "close";
      if (!res.error) {
        message = "successful";
        this.getPage(this.currentPage, 1);
      }
      swal(
        'Alert',
        'Data Deleted',
        'success'
      );
    });
  }
  getSavedFilter() {
    let param: any = { filter_type: this.currentFunction };
    this.httpService.get("/generic/saved_filter", param).subscribe((data) => {
      let res = data.json();
      this.savedFilter = res.data;
    });
  }
  doSaveFilter() {
    console.log('activeFiltername=>', this.activeFilterName);
    if (this.activeFilterName === "") {
    } else {
      let param: any = { filter_type: this.currentFunction, filters: JSON.stringify(this.search), filter_name: this.activeFilterName, filter_id: this.activeFilterId };
      this.httpService.post("/generic/saved_filter", param).subscribe((data) => {
        this.getSavedFilter();
      });
    }
  }
  doApplyFilter(filters) {
    this.search = JSON.parse(filters.filters);
    this.activeFilterId = filters.id;
    this.activeFilterName = filters.filter_name;
    this.getPage(1, 1);
  }
  doDeleteFilter() {
    this.httpService.delete("/generic/saved_filter/" + this.activeFilterId).subscribe((data) => {
      let res = data.json();
      let message = "failed";
      let action = "close";
      if (!res.error) {
        message = "successful";
        this.getSavedFilter();
      }
    });
  }
}
