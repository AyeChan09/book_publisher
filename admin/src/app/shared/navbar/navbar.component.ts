import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpService } from "../http.service";
import { Router } from '@angular/router';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent {
    currentLang = 'en';
    toggleClass = 'ft-maximize';
    constructor(
        public router:Router,
        public translate : TranslateService,
        public httpService : HttpService
    ) {
        const browserLang: string = translate.getBrowserLang();
        translate.use(browserLang.match(/en|es|pt|de/) ? browserLang : 'en');
    }

    ChangeLanguage(language: string) {
        this.translate.use(language);
    }

    ToggleClass() {
        if (this.toggleClass === 'ft-maximize') {
            this.toggleClass = 'ft-minimize';
        }
        else
            this.toggleClass = 'ft-maximize'
    }
    doLogout(){
        let rt : any = this.httpService.post("/auth/logout");
        rt.subscribe((data) =>{
            this.router.navigate(["/synapse/login"]);
        });
    }
}
