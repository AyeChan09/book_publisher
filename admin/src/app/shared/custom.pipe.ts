import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'arCurrency'
})
export class arCurrency implements PipeTransform {
  transform(value: any, args?: any): any {
    if (!value) return value;
    let currency = "S$";
    let decimal_point = 2;
    let ret = currency + " " + parseFloat(value).toFixed(decimal_point)
    // ret = ret.replace(".", ",");
    ret = ret.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    return ret;
  }
}
@Pipe({
  name: 'arNumber'
})
export class arNumber implements PipeTransform {
  transform(value: any, args?: any): any {
    if (!value) return value;
    let decimal_point = 2;
    let ret = parseFloat(value).toFixed(decimal_point)
    return ret;
  }
}
@Pipe({
  name: 'arDate'
})
export class arDate implements PipeTransform {
  transform(value: any, args?: any): any {
    let dateFormat = "DD/MM/YYYY";
    return moment(value).format(dateFormat);
    // return null;
  }

}

@Pipe({
  name: 'arDateTime'
})
export class arDateTime implements PipeTransform {
  transform(value: any, args?: any): any {
    let dateFormat = "DD/MM/YYYY hh:mm:ss a";
    return moment(value).format(dateFormat);
    // return null;
  }
}
@Pipe({
  name: 'arTime'
})
export class arTime implements PipeTransform {
  transform(value: any, args?: any): any { 
    let format="HH:mm::ss";
    let timeFormat = "h:mm A";
    return moment(value, format).format(timeFormat);    
  }
}

@Pipe({
  name: 'arTime1'
})
export class arTime1 implements PipeTransform {
  transform(value: any, args?: any): any { 
    let format="YYYY/MM/DD hh:mm:ss a";
    let timeFormat = "h:mm A";
    return moment(value, format).format(timeFormat);    
  }
}