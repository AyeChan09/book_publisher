import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { HttpService } from "./http.service";
import { ChartistModule } from 'ng-chartist';

import { FooterComponent } from "./footer/footer.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { CustomizerComponent } from './customizer/customizer.component';
import { ToggleFullscreenDirective } from "./directives/toggle-fullscreen.directive";
import { LocalStorageService } from "ngx-webstorage";
import { arCurrency,arDate,arTime,arTime1 } from './custom.pipe';
import { MomentModule } from 'angular2-moment';
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { arAddButton, arEditButton, arMoreButton, arDeleteButton, arViewButton, arSaveFilterButton, arClearFilterButton, arDeleteFilterButton, arSearchFilterButton, arSubmitButton, arBackButton, arPrintButton, arPDFButton, arEmailButton, arSelectButton, arWeekButton, arOkButton, arCartButton, arPaidButton,arSendButton, arGenerateFilterButton, arDeliveredButton } from "./component/button.component";
import { KeypadComponent } from './component/keypad/keypad.component';

import { LoadingModule } from 'ngx-loading';
import { SignaturePadModule } from 'angular2-signaturepad';
import { WebCamModule } from 'ack-angular-webcam';
@NgModule({
    exports: [
        CommonModule,
        FooterComponent,
        NavbarComponent,
        SidebarComponent,
        CustomizerComponent,
        LoadingModule,
        ToggleFullscreenDirective,
        NgbModule,
        TranslateModule,
        arCurrency,arDate,arTime,arTime1,
        MomentModule,      
        NgxPaginationModule,
        FormsModule,
        ReactiveFormsModule,
        QuillModule,
        ChartistModule,
        SignaturePadModule,
        WebCamModule,
        arAddButton,arEditButton,arMoreButton,arDeleteButton,arViewButton,arSaveFilterButton,arClearFilterButton,arDeleteFilterButton,arSearchFilterButton, arSubmitButton,arBackButton, arSelectButton, arPrintButton, arPDFButton, arEmailButton, arWeekButton,arOkButton,KeypadComponent,arCartButton,arPaidButton,arSendButton, arGenerateFilterButton, arDeliveredButton
    ],
    imports: [
        RouterModule,
        CommonModule,
        NgbModule,
        TranslateModule.forChild(),
        LoadingModule,
        CommonModule,
        MomentModule,      
        NgxPaginationModule,
        FormsModule,
        ReactiveFormsModule,
        ChartistModule,
        QuillModule,        
        SignaturePadModule,
        WebCamModule
    ],
    declarations: [
        FooterComponent,
        NavbarComponent,
        SidebarComponent,
        CustomizerComponent,
        ToggleFullscreenDirective,
        arCurrency,
        arDate,
        arTime,
        arTime1,
        arAddButton,arEditButton,arMoreButton,arDeleteButton,arViewButton,arSaveFilterButton,arClearFilterButton,arDeleteFilterButton,arSearchFilterButton, arSubmitButton,arBackButton, arSelectButton,arWeekButton, arPrintButton, arPDFButton, arEmailButton,arOkButton,KeypadComponent,arCartButton,arPaidButton,arSendButton,arGenerateFilterButton, arDeliveredButton
    ],
    providers:[HttpService,LocalStorageService]
})
export class SharedModule { }
