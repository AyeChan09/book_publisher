import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ar-keypad',
  templateUrl: './keypad.component.html',
  styleUrls: ['./keypad.component.scss']
})
export class KeypadComponent implements OnInit {

  qty: string = '';

  constructor(
    public ngbActiveModal : NgbActiveModal
  ) { }

  ngOnInit() {
  }

  doClick(input) {
    if(input=='back'){
      let lastIndex = this.qty.length - 1;
      this.qty = this.qty.substring(0, lastIndex);
    } else if(input=='clear'){
      this.qty = '';
    } else {
      this.qty += input;
    }
  }

  doSelect(){
    this.ngbActiveModal.close(parseInt(this.qty));
  }

}
