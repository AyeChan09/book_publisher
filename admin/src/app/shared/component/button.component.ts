import { Component, EventEmitter,Output } from '@angular/core';
@Component({
    selector:'ar-add-button',
    template: '<button type="button" class="btn btn-raised btn-info bg-lighten-4 white"><i class="fa fa-plus"></i> {{ "add" | translate }}</button>'
})export class arAddButton{}
@Component({
    selector:'ar-edit-button',
    template: '<button class="btn btn-outline-warning btn-xs"><i class="fa fa-pencil"></i></button>'
})export class arEditButton{}
@Component({
    selector:'ar-delete-button',
    template: '<button class="btn btn-outline-danger btn-xs"><i class="fa fa-trash"></i></button>'
})export class arDeleteButton{}
@Component({
    selector:'ar-view-button',
    template: '<button class="btn btn-outline-info btn-xs"><i class="fa fa-eye"></i></button>'
})export class arViewButton{}
@Component({
    selector:'ar-more-button',
    template: '<button class="btn btn-outline-primary btn-xs"><i class="fa fa-ellipsis-h"></i></button>'
})export class arMoreButton{}
@Component({
    selector:'ar-select-button',
    template: '<button class="btn btn-outline-success btn-xs"><i class="fa fa-check"></i></button>'
})export class arSelectButton{}
@Component({
    selector:'ar-search-filter-button',
    template: '<button class="btn btn-raised btn-info"><i class="fa fa-search"></i> {{ "search" | translate }}</button>'
})export class arSearchFilterButton{}
@Component({
    selector:'ar-clear-filter-button',
    template: '<button class="btn btn-raised btn-secondary"><i class="fa fa-eraser"></i> {{ "clear" | translate }}</button>'
})export class arClearFilterButton{}
@Component({
    selector:'ar-save-filter-button',
    template: '<button class="btn btn-raised btn-primary"><i class="fa fa-save"></i> {{ "save" | translate }}</button>'
})export class arSaveFilterButton{}
@Component({
    selector:'ar-delete-filter-button',
    template: '<button class="btn btn-raised btn-danger"><i class="fa fa-trash"></i> {{ "delete" | translate }}</button>'
})export class arDeleteFilterButton{}
@Component({
    selector:'ar-submit-button',
    template: '<button type="submit" class="btn btn-raised btn-success white"><i class="fa fa-check-square-o"></i>{{"submit" | translate}}</button>'
})export class arSubmitButton{}
@Component({
    selector:'ar-back-button',
    template: '<button type="button" class="btn btn-raised btn-raised gradient-indigo-blue white"><i class="fa fa-arrow-left"></i>{{"back" | translate}}</button>'
})export class arBackButton{}
@Component({
    selector:'ar-week-button',
    template: '<button class="btn btn-outline-info btn-xs"><i class="fa fa-calendar"></i></button>'
})export class arWeekButton{}
@Component({
    selector:'ar-ok-button',
    template: '<button type="button" class="btn btn-raised btn-primary"><i class="fa fa-check"></i> {{ "ok" | translate }}</button>'
})export class arOkButton{}
@Component({
    selector:'ar-print-button',
    template: '<button class="btn btn-raised btn-success"><i class="fa fa-fax"></i> {{ " print" | translate }}</button>'
})export class arPrintButton{}
@Component({
    selector:'ar-pdf-button',
    template: '<button class="btn btn-raised btn-xs"><i class="fa fa-print"></i> {{ " pdf" | translate }}</button>'
})export class arPDFButton{}
@Component({
    selector:'ar-email-button',
    template: '<button type="button" class="btn btn-raised btn-primary"><i class="fa fa-envelope-o"></i> {{ "email" | translate }}</button>'
})export class arEmailButton{}
@Component({
    selector:'ar-cart-button',
    template: '<button class="btn btn-outline-primary btn-xs"><i class="fa fa-shopping-cart"></i></button>'
})export class arCartButton{}
@Component({
    selector:'ar-paid-button',
    template: '<button class="btn btn-outline-primary btn-xs"><i class="fa fa-money"></i></button>'
})export class arPaidButton{}
@Component({
    selector:'ar-send-button',
    template: '<button type="submit" class="btn btn-raised btn-primary white"><i class="fa fa-send"></i>{{"send" | translate}}</button>'
})export class arSendButton{}
@Component({
    selector:'ar-generate-filter-button',
    template: '<button class="btn btn-raised btn-info"><i class="fa fa-share-square"></i> {{ "generate" | translate }}</button>'
})export class arGenerateFilterButton{}
@Component({
    selector:'ar-delivered-button',
    template: '<button class="btn btn-raised btn-success"><i class="far fa-shipping-fast"></i> {{ "delivered" | translate }}</button>'
})export class arDeliveredButton{}


