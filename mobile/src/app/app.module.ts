import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';

import { ConstantProvider } from '../providers/constant/constant';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { LocalServiceProvider } from '../providers/local-service/local-service';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { BookOnShelfPage } from '../pages/book-on-shelf/book-on-shelf';
import { CollectMoneyPage } from '../pages/collect-money/collect-money';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { BookOnShelfItemPage } from '../pages/book-on-shelf-item/book-on-shelf-item';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    BookOnShelfPage,
    CollectMoneyPage,
    ChangePasswordPage,
    BookOnShelfItemPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    BookOnShelfPage,
    CollectMoneyPage,
    ChangePasswordPage,
    BookOnShelfItemPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConstantProvider,
    ApiServiceProvider,
    LocalServiceProvider,
  ]
})
export class AppModule {}
