import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookOnShelfPage } from './book-on-shelf';
import { BookOnShelfItemPage } from '../book-on-shelf-item/book-on-shelf-item';

@NgModule({
  declarations: [
    BookOnShelfPage,
  ],
  imports: [
    IonicPageModule.forChild(BookOnShelfPage),
  ],
  entryComponents: [
    BookOnShelfItemPage
  ]
})
export class BookOnShelfPageModule {}
