import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { ConstantProvider } from '../../providers/constant/constant';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { BookOnShelfItemPage } from '../book-on-shelf-item/book-on-shelf-item';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'book-on-shelf'
})
@Component({
  selector: 'page-book-on-shelf',
  templateUrl: 'book-on-shelf.html',
})
export class BookOnShelfPage {
  invoiceForm: FormGroup;
  bookShops: any = [];
  data: any = [];
  items: FormArray;
  item_arr: any = [];
  id: number;
  distributor_id: number;
  isEdit: boolean;
  total_amount: number = 0;

  loading: Loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public apiService: ApiServiceProvider,
    public constantProvider: ConstantProvider,
    public formBuilder: FormBuilder,
    public modalCtrl: ModalController,) {
    this.apiService.post('book_shop/call_helper',{function:'book_shop'}).subscribe((data)=>{
      let res = data.json();
      this.bookShops = res;
      console.log('bookShop=>', this.bookShops);
    });
    
    this.distributor_id = parseInt(localStorage.getItem('user_id'));

    //FOR EDIT
    if(navParams.data.id){
      this.loading = this.constantProvider.loading();
      this.id = navParams.data.id;
      this.isEdit = true;
      this.apiService.get("invoice/edit/" + this.id).subscribe((data)=>{
        let res = data.json();               
        this.data = res.data;
        this.invoiceForm.patchValue(res.data);
        this.loading.dismiss();
      });
    }
    this.buildForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookOnShelfPage');
  }

  buildForm() {
    this.invoiceForm = this.formBuilder.group({
      voucher_no: ['', Validators.required],
      distributor_id: [this.distributor_id],
      book_shop_id: ['', Validators.required],
      total_amount: [''],
      items: this.formBuilder.array([], Validators.required),
    });
  }

  onSubmit({value, valid}) {
    if(valid){
      this.loading = this.constantProvider.loading();

      let parameter : any = {};
      parameter = value;
      
      if(!this.isEdit){
        let rt : any = this.apiService.post("invoice/invoice",parameter);
        rt.subscribe((data) =>{
          this.loading.dismiss();
          this.constantProvider.toast("invoice created!")
          this.navCtrl.setRoot(HomePage);
        });
      }else{
        let rt : any = this.apiService.put("invoice/invoice/" + this.id ,parameter);
        rt.subscribe((data) =>{
          this.loading.dismiss();
          this.constantProvider.toast("invoice updated!");
          this.navCtrl.setRoot(HomePage);
        });
      }
    }
  }


  //ADD NEW BOOK
  addBook(index){
    let modal = this.modalCtrl.create(BookOnShelfItemPage, {data: this.item_arr});
    modal.onDidDismiss(data =>{
      console.log('data from modal=>', data);
      if(data != null){
        this.doAddBook(data);
        this.total_amount += data.total;
        console.log('total=>', this.total_amount);
      } 
    });
    modal.present();
  }

  bookForm(item: any = {}): FormGroup {
    let bookForm = this.formBuilder.group({
      id: item.id,     
      book_name: [item.book_name],     
      invoice_id: [item.invoice_id],
      book_id: [item.book_id],
      qty: [item.qty],
      price: [item.price],
      total: [item.total]
    });
    return bookForm;
  }

  doAddBook(item: any = {}) {
    this.items = <FormArray>this.invoiceForm.get('items');
    this.items.push(this.bookForm(item));
    this.item_arr.push(item);
  }

  //DELETE BOOK
  doDeleteBook(index) {
    if(this.item_arr.length > 1){
      this.items = <FormArray>this.invoiceForm.get('items');
      console.log('items=>', this.items);
      console.log('item_arr->', this.item_arr[index]);
      this.total_amount -= this.item_arr[index].total;
      this.items.removeAt(index);
      this.item_arr.splice(index,1);
    }
  }

  deletedBook(id): FormGroup {
    return this.formBuilder.group({ id: id });
  }

  //EDIT BOOK
  doEditBook(item: any = {}, index) {
    this.items.at(index).patchValue(item);    
    this.item_arr[index] = item;
  }
  
}
