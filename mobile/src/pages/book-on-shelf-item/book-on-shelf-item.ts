import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Loading } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { ConstantProvider } from '../../providers/constant/constant';

@IonicPage()
@Component({
  selector: 'page-book-on-shelf-item',
  templateUrl: 'book-on-shelf-item.html',
})
export class BookOnShelfItemPage {
  bookForm: FormGroup;
  loading: Loading;
  books: any = [];
  book: any = {};
  invoice_id: number;
  qty: number = 0;
  price: number = 0;
  total: number = 0;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public apiService: ApiServiceProvider,
    public constantProvider: ConstantProvider,
    private viewCtrl: ViewController,
    ) {
      this.apiService.post('book/call_helper',{function:'book'}).subscribe((data)=>{
        let res = data.json();
        this.books = res;
      });

      this.buildForm();
  }

  buildForm() {
    this.bookForm = this.formBuilder.group({
      id: [''],
      invoice_id: [this.invoice_id],
      book_name: [''],
      book_id: ['', Validators.required],
      qty: ['', Validators.required],
      price: ['', Validators.required],
      total: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookOnShelfItemPage');
  }

  // addBook(bookForm: FormGroup):void {
  //   this.viewCtrl.dismiss(bookForm);
  // }

  addBook({value, valid}) {
    console.log('book=>', this.book);
    if(valid){
      console.log('valid');
      this.loading = this.constantProvider.loading();

      let parameter : any = {};
      parameter = value;
      console.log('parameter=>', parameter);
      parameter['book_name'] = this.book.name;
      parameter['book_id'] = this.book.id;
      console.log('parameter after =>', parameter);
      this.loading.dismiss();
      this.viewCtrl.dismiss(parameter);
      
    } else {
      console.log('invalid');
    }
  }

  sum() {
    this.total = this.qty * this.price;
  }

}
