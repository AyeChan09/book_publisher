import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookOnShelfItemPage } from './book-on-shelf-item';

@NgModule({
  declarations: [
    BookOnShelfItemPage,
  ],
  imports: [
    IonicPageModule.forChild(BookOnShelfItemPage),
  ],
})
export class BookOnShelfItemPageModule {}
