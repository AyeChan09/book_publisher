import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { ConstantProvider } from '../../providers/constant/constant';
import { LocalServiceProvider } from '../../providers/local-service/local-service';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  data: any = {};
  loginForm: FormGroup;
  loading: Loading;
  seen: boolean = true;
  pwInputType = 'password';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public constantProvider: ConstantProvider,
    public apiService: ApiServiceProvider,
    public localService: LocalServiceProvider,
  ) {
    this.buildForm();
  }

  buildForm() {
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.required],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  login({ value, valid }) {
    if (valid) {
      this.loading = this.constantProvider.loading();
      let parameter: any = {};
      parameter = value;
      this.apiService.post("distributor/login", parameter).subscribe(data => {
        this.data = data.json();
        if (this.data.error == true) {
          this.loading.dismiss().then(() => {
            this.constantProvider.basicAlert("Error!", "Invalid login. Your Name and password are wrong.");
          })
        } else {
          this.loading.dismiss().then(() => {        
            // this.navCtrl.setRoot("home");
            this.navCtrl.setRoot(HomePage);
            localStorage.setItem("user_id", this.data.data.id);
          });
        }
      })
    }
  }

  seePassword() {
    this.seen = !this.seen;
    if (this.seen) {
      this.pwInputType = "password"
    } else {
      this.pwInputType = "text"
    }
  }
}
