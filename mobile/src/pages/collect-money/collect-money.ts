import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { ConstantProvider } from '../../providers/constant/constant';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'collect-money'
})
@Component({
  selector: 'page-collect-money',
  templateUrl: 'collect-money.html',
})
export class CollectMoneyPage {
  collectMoneyForm: FormGroup;
  bookShops: any = [];
  vouchers: any = [];
  status: any = ['half', 'full'];
  balance: number = 0;
  ori_balance: number = 0;
  book_shop: number = 0;

  loading: Loading;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public apiService: ApiServiceProvider,
    public constantProvider: ConstantProvider,
    public formBuilder: FormBuilder,) {
      this.apiService.post('book_shop/call_helper',{function:'book_shop'}).subscribe((data)=>{
        let res = data.json();
        this.bookShops = res;
      });
      this.buildForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CollectMoneyPage');
  }

  getBookShop(event) {
    console.log('event=>', event);
    this.book_shop = event;
    this.apiService.get('invoice/get_voucher/'+ event).subscribe((data)=>{
      let res = data.json();
      this.vouchers = res;
      console.log('voucher=>', this.vouchers);
    });
  }

  getBalance(event) {
    console.log('event=>', event);
    let param: any = {};
    param['voucher_no'] = event;
    param['book_shop_id'] = this.book_shop;
    this.apiService.get('invoice/get_balance', param).subscribe((data)=>{
      console.log('event=>', data);
      let res = data.json();
      this.balance = res;
      this.ori_balance = res;
      
    });
  }

  setPayment(event) {
    console.log('payment=>', event);
    let payment = this.collectMoneyForm.get('payment_amount').value;
    if(payment > this.ori_balance) {
      this.collectMoneyForm.patchValue({
        payment_amount: this.ori_balance
      });
      this.balance = 0;
    } else {
      this.balance = this.ori_balance - payment;
    }
      
  }

  buildForm() {
    this.collectMoneyForm = this.formBuilder.group({
      book_shop_id: ['', Validators.required],
      voucher_no: ['', Validators.required],
      payment_amount: ['', Validators.required],
      balance: ['', Validators.required],
    });
  }

  onSubmit({value, valid}) {
    if(valid){
        this.loading = this.constantProvider.loading();
  
        let parameter : any = {};
        parameter = value;
        if(parameter['payment_amount'] < 1) {
          this.constantProvider.basicAlert("Notice !", "No Payment Amount.");
          this.loading.dismiss();
        } else {
          let rt : any = this.apiService.post("credit/credit",parameter);
          rt.subscribe((data) =>{
            this.loading.dismiss();
            this.constantProvider.toast("Credit created!");
            this.navCtrl.setRoot(HomePage);
          });
        }
      }
    }

}
