import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CollectMoneyPage } from './collect-money';

@NgModule({
  declarations: [
    CollectMoneyPage,
  ],
  imports: [
    IonicPageModule.forChild(CollectMoneyPage),
  ],
})
export class CollectMoneyPageModule {}
