import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { BookOnShelfPage } from '../book-on-shelf/book-on-shelf';
import { CollectMoneyPage } from '../collect-money/collect-money';
import { ChangePasswordPage } from '../change-password/change-password';

@IonicPage({
  name: 'home'
})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  bookOnShelf() {
    console.log('bookOnShelf');
    this.navCtrl.push(BookOnShelfPage);
  }

  collectMoney() {
    console.log('collectMoney');
    this.navCtrl.push(CollectMoneyPage);
  }

  changePassword() {
    this.navCtrl.push(ChangePasswordPage);
  }
}
