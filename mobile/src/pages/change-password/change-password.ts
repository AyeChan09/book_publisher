import { Component } from '@angular/core';
import { IonicPage, NavController, Platform, Loading } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { ConstantProvider } from '../../providers/constant/constant';

@IonicPage({
  name: 'change-password'
})
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {
  changePasswordForm: FormGroup;
  loading: Loading;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public formBuilder: FormBuilder,
    public apiService: ApiServiceProvider,
    public constantProvider: ConstantProvider
  ) {
    platform.registerBackButtonAction(() => {      
        this.loading.dismiss();  
        this.navCtrl.pop();       
    });
    this.buildForm();
  }

  buildForm(){
    this.changePasswordForm = this.formBuilder.group({
      current_password: ['', Validators.required],
      new_password: ['', Validators.compose([Validators.required])],
      confirm_password: ['', Validators.compose([Validators.required])]
    });
  }

  onSubmit({value, valid}) {
    if(valid){
      this.loading = this.constantProvider.loading();

      let parameter: any = {};
      parameter = value;

      if(this.changePasswordForm.value.current_password != localStorage.getItem("password")) {
        this.loading.dismiss().then(() => {
          this.constantProvider.basicAlert("Error!", "Your current password are wrong.");
        });
      } else if(this.changePasswordForm.value.new_password != this.changePasswordForm.value.confirm_password) {        
        this.loading.dismiss().then(() => {
          this.constantProvider.basicAlert("Error!", "Your confirm password must be same to new password.");
        });      
      } else {
        let newPassword = this.changePasswordForm.value.confirm_password;
        this.apiService.get("distributor/change_password/" + parseInt(localStorage.getItem("user_id")), {'newPassword': newPassword}).subscribe(data => {
          let res = data.json();
          if (res.error == true) {
            this.loading.dismiss().then(() => {
              this.constantProvider.basicAlert("Error", "Invalid Changing Password.");
            })
          } else {
            this.loading.dismiss().then(() => {
              this.constantProvider.toast("Password changed!");
              localStorage.setItem("password", res.password); 
              this.navCtrl.push("setting");
            });
          }
        });
      }     
    }
    
  }
}
