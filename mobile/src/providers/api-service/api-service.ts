import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { ConstantProvider } from '../constant/constant';

@Injectable()
export class ApiServiceProvider {

  apiPath = "";
  params = {};
  url = "";

  constructor(
    public http: Http,
    public constantProvider: ConstantProvider
  ) {
    this.apiPath = this.constantProvider.getApiPath();
  }

  get(url, parameter = {}, headers = {}){
    if(headers['Content-Type']!=="undefined"){
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers : httpHeader, params:parameter  });
    return this.http.get(this.apiPath + url, options);    
  }

  post(url, parameter = {}, headers = {}){
    if(headers['Content-Type']!=="undefined"){
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers : httpHeader});  
    return this.http.post(this.apiPath+url, parameter, options);    
  }

  put(url, parameter = {}, headers = {}){
    if(headers['Content-Type']!=="undefined"){
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers : httpHeader });
  
    return this.http.put(this.apiPath+url, parameter, options);
  }

  delete(url, parameter = {}, headers = {}){
    if(headers['Content-Type']!=="undefined"){
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers : httpHeader });
   
    return this.http.delete(this.apiPath+url, options);
  }

}
