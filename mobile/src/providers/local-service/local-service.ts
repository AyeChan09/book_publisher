import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

@Injectable()
export class LocalServiceProvider {

  user: any = [];

  constructor(
    public http: Http,
    public storage: Storage) {
    // this.storage.ready().then(() => {
    //   this.checkData('user', this.user);
    // });
  }

  checkData(key, value) {
    this.storage.get(key).then(data => {
      if (data == null) {
        this.storage.set(key, value);
      } else {
        this.storage.get(key);
      }
    });
  }

  getData(key) {
    return this.storage.get(key);
  }

  addData(key, value) {
    this.storage.set(key, value);
    this.user = value;
  }

  removeData(key, index) {
    this.storage.get(key).then(data => {
      this.user.splice(index, 1);
      this.storage.set(key, this.user);
    });
  }

  clearData(key) {
    this.storage.remove(key);
  }

  loadData(key) {
    this.storage.get(key).then(data => {
      console.log('storage ' + key, data);
    });
  }
}
