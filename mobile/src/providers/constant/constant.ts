import { Injectable } from '@angular/core';
import { AlertController, ToastController, LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';

@Injectable()
export class ConstantProvider {

  // public serverPath = "https://arzaphosting2.biz/prototype/teacher/eduBackend/public/";
  // public serverPath = "http://192.168.1.9/work_project/eduTeacherApp/eduBackend/public/";
  public serverPath = "http://localhost:8000/";
  public apiPath = this.serverPath;
  public homePageIndex = 0;
  public currentPageIndex = 0;

  constructor(
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadCtrl: LoadingController,
  ) {
  }

  public getServerPath() {
    return this.serverPath;
  }

  public getApiPath() {
    return this.apiPath;
  }

  basicAlert(title, msg) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  confirmAlert(title, msg, text1, text2) {
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create({
        title: title,
        message: msg,
        buttons: [
          {
            text: text1,
            handler: () => {
              resolve(true);
            }
          },
          {
            text: text2,
            role: 'cancel',
            handler: () => {
              resolve(false);
            }
          }
        ]
      });
      alert.present();
    });
  }

  radioAlert(title: string, array: any) {
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create();
      alert.setTitle(title);
      array.forEach((item, index) => {
        alert.addInput({
          type: 'radio',
          label: item.name,
          value: item.value,
          checked: item.status
        });
      });

      alert.addButton({
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          resolve('cancel');
        }
      });
      alert.addButton({
        text: 'Ok',
        handler: (data: any) => {
          resolve(data);
        }
      });

      alert.present();
    })
  }

  toast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2500,
      position: 'top'
    });
    toast.present();
  }

  loading() {
    let load = this.loadCtrl.create({
      spinner: 'hide',
      content: `<img src="assets/icon/loadingIcon.gif" />`,
      dismissOnPageChange: false
    });
    load.present();
    return load;
  }

}
