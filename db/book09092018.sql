-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for book_publisher
CREATE DATABASE IF NOT EXISTS `book_publisher` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `book_publisher`;

-- Dumping structure for table book_publisher.acc_admin_menu_item
CREATE TABLE IF NOT EXISTS `acc_admin_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `is_show` tinyint(4) DEFAULT NULL,
  `path` varchar(255) DEFAULT '',
  `external_href` varchar(255) DEFAULT NULL,
  `external_href_target` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT '',
  `class` varchar(255) DEFAULT '',
  `order_id` int(11) DEFAULT NULL,
  `has_child` tinyint(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.acc_admin_menu_item: ~8 rows (approximately)
/*!40000 ALTER TABLE `acc_admin_menu_item` DISABLE KEYS */;
INSERT INTO `acc_admin_menu_item` (`id`, `title`, `is_show`, `path`, `external_href`, `external_href_target`, `icon`, `class`, `order_id`, `has_child`, `parent_id`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(1, 'Book', 1, '', NULL, NULL, 'fa fa-book', '', 1, 1, 0, '2018-08-01 22:10:19', NULL, NULL, NULL),
	(2, 'Book List', 1, '/synapse/book/book_list', NULL, NULL, '', '', 1, 0, 1, '2018-08-01 22:10:19', NULL, NULL, NULL),
	(3, 'Book Shop', 1, '', NULL, NULL, 'fa fa-home', '', 2, 1, 0, '2018-08-01 22:10:19', NULL, NULL, NULL),
	(4, 'Book Shop List', 1, '/synapse/book_shop/book_shop_list', NULL, NULL, '', '', 1, 0, 3, '2018-08-01 22:10:19', NULL, NULL, NULL),
	(5, 'Distributor', 1, '', NULL, NULL, 'fa fa-users', '', 3, 1, 0, '2018-08-01 22:10:19', NULL, NULL, NULL),
	(6, 'Distributor List', 1, '/synapse/distributor/distributor_list', NULL, NULL, '', '', 1, 0, 5, '2018-08-01 22:10:19', NULL, NULL, NULL),
	(7, 'Report', 1, '', NULL, NULL, 'fa fa-list', '', 4, 1, 0, '2018-08-01 22:10:19', NULL, NULL, NULL),
	(8, 'Sale Report', 1, '/synapse/report/sale', NULL, NULL, '', '', 1, 0, 7, '2018-08-01 22:10:19', NULL, NULL, NULL);
/*!40000 ALTER TABLE `acc_admin_menu_item` ENABLE KEYS */;

-- Dumping structure for table book_publisher.acc_admin_menu_item_option
CREATE TABLE IF NOT EXISTS `acc_admin_menu_item_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acc_admin_menu_item_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.acc_admin_menu_item_option: ~9 rows (approximately)
/*!40000 ALTER TABLE `acc_admin_menu_item_option` DISABLE KEYS */;
INSERT INTO `acc_admin_menu_item_option` (`id`, `acc_admin_menu_item_id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 2, 'allow_add', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 2, 'allow_edit', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 2, 'allow_delete', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 4, 'allow_add', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 4, 'allow_edit', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 4, 'allow_delete', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 6, 'allow_add', NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 6, 'allow_edit', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 6, 'allow_delete', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `acc_admin_menu_item_option` ENABLE KEYS */;

-- Dumping structure for table book_publisher.acc_list_setting
CREATE TABLE IF NOT EXISTS `acc_list_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `field_name` varchar(255) DEFAULT NULL,
  `type` enum('string','currency','date') DEFAULT NULL,
  `is_show` tinyint(4) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.acc_list_setting: ~12 rows (approximately)
/*!40000 ALTER TABLE `acc_list_setting` DISABLE KEYS */;
INSERT INTO `acc_list_setting` (`id`, `list_name`, `label`, `field_name`, `type`, `is_show`, `order_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 'book', 'name', 'name', 'string', 1, 1, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL),
	(2, 'book', 'author', 'author', 'string', 1, 2, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL),
	(3, 'book', 'price', 'price', 'string', 1, 3, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL),
	(4, 'book_shop', 'name', 'name', 'string', 1, 1, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL),
	(5, 'book_shop', 'contact_person', 'contact_person', 'string', 1, 2, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL),
	(6, 'book_shop', 'phone', 'phone', 'string', 1, 3, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL),
	(7, 'book_shop', 'address', 'address', 'string', 1, 4, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL),
	(8, 'distributor', 'name', 'name', 'string', 1, 1, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL),
	(9, 'distributor', 'phone', 'phone', 'string', 1, 2, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL),
	(10, 'distributor', 'gender', 'gender', 'string', 1, 3, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL),
	(11, 'distributor', 'nrc', 'nrc', 'string', 1, 4, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL),
	(12, 'distributor', 'address', 'address', 'string', 1, 5, '2018-08-01 22:39:19', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `acc_list_setting` ENABLE KEYS */;

-- Dumping structure for table book_publisher.book
CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.book: ~5 rows (approximately)
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` (`id`, `name`, `author`, `price`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 'Web Developer', 'Ei Maung', '8000', '2018-08-01 23:08:02', NULL, NULL, NULL, NULL, NULL),
	(2, 'Rock Star', 'Ei Maung', '8000', '2018-08-01 16:53:42', 'guest', '2018-08-01 16:55:23', 'guest', NULL, NULL),
	(4, 'JavaScript', 'Yan Hmue Aung', '5000', '2018-08-01 17:37:21', 'guest', '2018-08-01 17:38:38', 'guest', NULL, NULL),
	(5, 'Laravel', 'Aye Chan Thaw', '10000', '2018-08-01 17:38:09', 'guest', '2018-08-01 17:38:09', NULL, NULL, NULL),
	(6, 'Don\'t wanna tell to Darling', 'Pu Nya Khin', '3500', '2018-08-18 01:58:21', 'guest', '2018-08-18 01:58:21', NULL, NULL, NULL);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;

-- Dumping structure for table book_publisher.book_shop
CREATE TABLE IF NOT EXISTS `book_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.book_shop: ~3 rows (approximately)
/*!40000 ALTER TABLE `book_shop` DISABLE KEYS */;
INSERT INTO `book_shop` (`id`, `name`, `address`, `phone`, `contact_person`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 'Innwa 2', 'Hle Dan', '9798876786', 'Daw Hla Win', '2018-08-02 01:03:25', NULL, '2018-08-01 18:50:44', 'guest', NULL, NULL),
	(3, 'Yar Pyae', 'Pan Soe Dan', '979475849387', 'Daw Hla Tin', '2018-08-01 18:51:28', 'guest', '2018-08-01 18:51:28', NULL, NULL, NULL),
	(4, 'Innwa (Hledan)', 'Hle Dan', '96786876', 'Daw Hla Tin', '2018-08-18 01:59:09', 'guest', '2018-08-18 01:59:09', NULL, NULL, NULL);
/*!40000 ALTER TABLE `book_shop` ENABLE KEYS */;

-- Dumping structure for table book_publisher.credit
CREATE TABLE IF NOT EXISTS `credit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `voucher_no` varchar(255) DEFAULT NULL,
  `book_shop_id` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `status` enum('unpaid','half','paid') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.credit: ~3 rows (approximately)
/*!40000 ALTER TABLE `credit` DISABLE KEYS */;
INSERT INTO `credit` (`id`, `invoice_id`, `voucher_no`, `book_shop_id`, `total`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 2, '7979jkj', 3, 8000, 'half', '2018-09-08 19:02:38', 'guest', '2018-09-08 19:02:38', NULL, NULL, NULL),
	(2, 2, '7979jkj', 3, 6000, 'half', '2018-09-08 19:03:18', 'guest', '2018-09-08 19:03:18', NULL, NULL, NULL),
	(3, 3, 'fef44', 4, 5000, 'half', '2018-09-08 19:27:49', 'guest', '2018-09-08 19:27:49', NULL, NULL, NULL);
/*!40000 ALTER TABLE `credit` ENABLE KEYS */;

-- Dumping structure for table book_publisher.distributor
CREATE TABLE IF NOT EXISTS `distributor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `nrc` varchar(50) DEFAULT NULL,
  `gender` enum('male','female','other') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.distributor: ~3 rows (approximately)
/*!40000 ALTER TABLE `distributor` DISABLE KEYS */;
INSERT INTO `distributor` (`id`, `name`, `password`, `phone`, `address`, `nrc`, `gender`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 'Mg Arkar', '123456', '9890877', 'Kyint Myin Tine', '12/ThaLaNa(N)678685', 'male', '2018-08-01 19:42:35', 'guest', '2018-08-01 19:44:05', 'guest', NULL, NULL),
	(2, 'Ma Saw Mya', '123456', '9688978', NULL, '09/MaAh(N)78799', NULL, '2018-08-01 19:45:01', 'guest', '2018-08-01 19:45:01', NULL, NULL, NULL),
	(3, 'Mg Khant Zin', NULL, '96786879', 'Thanlyin', '12/ThaLaNa(N)117899', 'male', '2018-08-18 02:00:10', 'guest', '2018-08-18 02:00:10', NULL, NULL, NULL);
/*!40000 ALTER TABLE `distributor` ENABLE KEYS */;

-- Dumping structure for table book_publisher.invoice
CREATE TABLE IF NOT EXISTS `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_no` varchar(255) DEFAULT NULL,
  `distributor_id` int(11) DEFAULT NULL,
  `book_shop_id` int(11) DEFAULT NULL,
  `total_amount` int(11) DEFAULT NULL,
  `status` enum('none','half-receive','receive') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.invoice: ~3 rows (approximately)
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` (`id`, `voucher_no`, `distributor_id`, `book_shop_id`, `total_amount`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 'hh79789', 1, 1, 130000, 'none', '2018-08-19 09:12:45', 'guest', '2018-08-19 09:12:45', NULL, NULL, NULL),
	(2, '7979jkj', 1, 3, 115000, 'receive', '2018-08-19 09:25:02', 'guest', '2018-08-19 09:25:02', NULL, NULL, NULL),
	(3, 'fef44', 1, 4, 90000, 'half-receive', '2018-08-19 09:29:44', 'guest', '2018-09-08 19:27:49', 'guest', NULL, NULL);
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;

-- Dumping structure for table book_publisher.invoice_item
CREATE TABLE IF NOT EXISTS `invoice_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.invoice_item: ~6 rows (approximately)
/*!40000 ALTER TABLE `invoice_item` DISABLE KEYS */;
INSERT INTO `invoice_item` (`id`, `invoice_id`, `book_id`, `qty`, `price`, `total`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 1, 2, 10, 5000, 50000, '2018-08-19 09:12:45', NULL, '2018-08-19 09:12:45', NULL, NULL, NULL),
	(2, 1, 1, 10, 8000, 80000, '2018-08-19 09:12:46', NULL, '2018-08-19 09:12:46', NULL, NULL, NULL),
	(3, 2, 1, 10, 8000, 80000, '2018-08-19 09:25:02', NULL, '2018-08-19 09:25:02', NULL, NULL, NULL),
	(4, 2, 6, 10, 3500, 35000, '2018-08-19 09:25:02', NULL, '2018-08-19 09:25:02', NULL, NULL, NULL),
	(5, 3, 4, 10, 3000, 30000, '2018-08-19 09:29:44', NULL, '2018-08-19 09:29:44', NULL, NULL, NULL),
	(6, 3, 5, 5, 12000, 60000, '2018-08-19 09:29:44', NULL, '2018-08-19 09:29:44', NULL, NULL, NULL);
/*!40000 ALTER TABLE `invoice_item` ENABLE KEYS */;

-- Dumping structure for table book_publisher.lang_list
CREATE TABLE IF NOT EXISTS `lang_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.lang_list: ~5 rows (approximately)
/*!40000 ALTER TABLE `lang_list` DISABLE KEYS */;
INSERT INTO `lang_list` (`id`, `name`, `abbrev`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'English', 'en', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Chinese', 'zh', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Indonesia', 'ind', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'Malaysia', 'my', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'Myanmar', 'mm', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `lang_list` ENABLE KEYS */;

-- Dumping structure for table book_publisher.lang_presentation
CREATE TABLE IF NOT EXISTS `lang_presentation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template` varchar(255) DEFAULT NULL,
  `en` varchar(255) DEFAULT NULL,
  `zh` varchar(255) DEFAULT NULL,
  `ind` varchar(255) DEFAULT NULL,
  `vi` varchar(255) DEFAULT NULL,
  `mm` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.lang_presentation: ~23 rows (approximately)
/*!40000 ALTER TABLE `lang_presentation` DISABLE KEYS */;
INSERT INTO `lang_presentation` (`id`, `template`, `en`, `zh`, `ind`, `vi`, `mm`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'login', 'Login', 'Login', 'Login', 'Login', 'Login', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'number', 'Number', 'Number', 'Number', 'Number', 'Number', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'search', 'Search', 'Search', 'Search', 'Search', 'Search', '2018-01-03 00:17:42', 'guest', '2018-01-03 00:17:42', NULL, NULL, NULL, NULL),
	(4, 'filter_name', 'Filter Name', 'Filter Name', 'Filter Name', 'Filter Name', 'Filter Name', '2018-01-03 00:17:42', 'guest', '2018-01-03 00:17:42', NULL, NULL, NULL, NULL),
	(5, 'active_filter', 'Active Filter', 'Active Filter', 'Active Filter', 'Active Filter', 'Active Filter', '2018-01-03 00:17:42', 'guest', '2018-01-03 00:17:42', NULL, NULL, NULL, NULL),
	(6, 'user_group', 'User Group', 'User Group', 'User Group', 'User Group', 'User Group', '2018-01-03 00:17:42', 'guest', '2018-01-03 00:17:42', NULL, NULL, NULL, NULL),
	(7, 'from_Date', 'From Date', 'From Date', 'From Date', 'From Date', 'From Date', '2018-01-03 00:17:43', 'guest', '2018-01-03 00:17:43', NULL, NULL, NULL, NULL),
	(8, 'to_date', 'To Date', 'To Date', 'To Date', 'To Date', 'To Date', '2018-01-03 00:17:43', 'guest', '2018-01-03 00:17:43', NULL, NULL, NULL, NULL),
	(9, 'saved_search', 'Saved Search', 'Saved Search', 'Saved Search', 'Saved Search', 'Saved Search', '2018-01-03 00:17:43', 'guest', '2018-01-03 00:17:43', NULL, NULL, NULL, NULL),
	(10, 'sort_by', 'Sort By', 'Sort By', 'Sort By', 'Sort By', 'Sort By', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(11, 'sort_dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(12, 'ascending', 'Ascending', 'Ascending', 'Ascending', 'Ascending', 'Ascending', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(13, 'descending', 'Descending', 'Descending', 'Descending', 'Descending', 'Descending', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(14, 'action', 'Action', 'Action', 'Action', 'Action', 'Action', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(15, 'clear', 'Clear', 'Clear', 'Clear', 'Clear', 'Clear', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(16, 'save', 'Save', 'Save', 'Save', 'Save', 'Save', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL),
	(17, 'delete', 'Delete', 'Delete', 'Delete', 'Delete', 'Delete', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL),
	(18, 'Book', 'Book', 'Book', 'Book', 'Book', 'Book', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL),
	(19, 'Book  List', 'Book  List', 'Book  List', 'Book  List', 'Book  List', 'Book  List', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL),
	(20, 'Book Shop', 'Book Shop', 'Book Shop', 'Book Shop', 'Book Shop', 'Book Shop', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL),
	(21, 'Book Shop  List', 'Book Shop  List', 'Book Shop  List', 'Book Shop  List', 'Book Shop  List', 'Book Shop  List', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL),
	(22, 'Distributor', 'Distributor', 'Distributor', 'Distributor', 'Distributor', 'Distributor', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL),
	(23, 'Distributor  List', 'Distributor  List', 'Distributor  List', 'Distributor  List', 'Distributor  List', 'Distributor  List', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `lang_presentation` ENABLE KEYS */;

-- Dumping structure for table book_publisher.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table book_publisher.migrations: ~2 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table book_publisher.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table book_publisher.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table book_publisher.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `code`, `username`, `password`, `email`, `mobile`, `gender`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, '1', 'root', '$2y$10$wlmVNq5ClZBiMk.9ogoVJusYk8R3mjECI6SrBBMKFX1DeexOq4UNu', 'root@root.com', '96369079', 'Female', NULL, NULL, '2017-12-22 17:51:14', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table book_publisher.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table book_publisher.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table book_publisher.user_access
CREATE TABLE IF NOT EXISTS `user_access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `acc_admin_menu_id` int(11) DEFAULT NULL,
  `acc_admin_menu_option_id` int(11) DEFAULT NULL,
  `access_type` enum('none','full','custom') DEFAULT NULL,
  `custom_access` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.user_access: ~9 rows (approximately)
/*!40000 ALTER TABLE `user_access` DISABLE KEYS */;
INSERT INTO `user_access` (`id`, `user_id`, `acc_admin_menu_id`, `acc_admin_menu_option_id`, `access_type`, `custom_access`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 1, 2, 1, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 1, 2, 2, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 1, 2, 3, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 1, 4, 4, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 1, 4, 5, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 1, 4, 6, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 1, 6, 4, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 1, 6, 5, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 1, 6, 6, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `user_access` ENABLE KEYS */;

-- Dumping structure for table book_publisher.user_saved_filter
CREATE TABLE IF NOT EXISTS `user_saved_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `filter_type` varchar(255) DEFAULT NULL,
  `filter_name` varchar(255) DEFAULT NULL,
  `filters` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.user_saved_filter: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_saved_filter` DISABLE KEYS */;
INSERT INTO `user_saved_filter` (`id`, `user_id`, `filter_type`, `filter_name`, `filters`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(2, 1, 'book', 'java', '{"name":"java"}', '2018-08-01 18:23:22', NULL, '2018-08-01 18:23:22', NULL, NULL, NULL),
	(3, 1, 'distributor', 'search', '{"name":"Saw"}', '2018-08-01 19:45:17', NULL, '2018-08-01 19:45:17', NULL, NULL, NULL);
/*!40000 ALTER TABLE `user_saved_filter` ENABLE KEYS */;

-- Dumping structure for table book_publisher.user_session
CREATE TABLE IF NOT EXISTS `user_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `logout_time` datetime DEFAULT NULL,
  `last_action` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Dumping data for table book_publisher.user_session: ~29 rows (approximately)
/*!40000 ALTER TABLE `user_session` DISABLE KEYS */;
INSERT INTO `user_session` (`id`, `user_id`, `token`, `ip_address`, `login_time`, `logout_time`, `last_action`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 1, 'W9dvOLhxteFne5wDWv51tUMabHeeFxlNzrCCQfyg5NzH0jjfne', '127.0.0.1', '2018-07-31 16:05:00', NULL, '2018-07-31 16:05:00', '2018-07-31 16:05:02', 'System', '2018-07-31 16:05:02', NULL, NULL, NULL),
	(2, 1, 'J3emhTb2jfKVOX2HRFtcuIPFUb7YT6uq7ZINfOCGM19vF1O7EJ', '127.0.0.1', '2018-07-31 16:05:57', NULL, '2018-07-31 16:05:57', '2018-07-31 16:05:57', 'System', '2018-07-31 16:05:57', NULL, NULL, NULL),
	(3, 1, 'QeL3FkgEaamG0ldbHUbjY2vsag8bQ0k2MHkBCvxmMmCOLYUWWL', '127.0.0.1', '2018-07-31 16:07:24', NULL, '2018-07-31 16:07:24', '2018-07-31 16:07:24', 'System', '2018-07-31 16:07:24', NULL, NULL, NULL),
	(4, 1, 'pvp5XZR5gkCaOjA6gXvtM8y0TZfRF9DnZXsApWMr00hL7ifDdp', '127.0.0.1', '2018-07-31 16:07:51', NULL, '2018-07-31 16:07:51', '2018-07-31 16:07:51', 'System', '2018-07-31 16:07:51', NULL, NULL, NULL),
	(5, 1, 'QR8aET6y4GyfMGBl2MnFZzCHqjEnbQ9yhlBVyWLdf2LXPQF4tC', '127.0.0.1', '2018-07-31 16:09:16', NULL, '2018-07-31 16:09:16', '2018-07-31 16:09:16', 'System', '2018-07-31 16:09:16', NULL, NULL, NULL),
	(6, 1, 'aficnFg61VkcTKb2qFO0gJiLSqjhDZYPlJIUyl1f4g249TwOWa', '127.0.0.1', '2018-07-31 16:23:32', NULL, '2018-07-31 16:23:32', '2018-07-31 16:23:32', 'System', '2018-07-31 16:23:32', NULL, NULL, NULL),
	(7, 1, 'lOY11u8IfvQ3fWksYawOSLziSS4NElGrN7MSZg2ZLOhO84bCUs', '127.0.0.1', '2018-07-31 16:25:18', NULL, '2018-07-31 16:25:18', '2018-07-31 16:25:18', 'System', '2018-07-31 16:25:18', NULL, NULL, NULL),
	(8, 1, 'ztWQIt1roSLQ17L5i1bgvoYCWiqEGsRAOyti77bHi8j5xEmdfU', '127.0.0.1', '2018-07-31 16:35:29', NULL, '2018-07-31 16:35:29', '2018-07-31 16:35:30', 'System', '2018-07-31 16:35:30', NULL, NULL, NULL),
	(9, 1, 'VTrXpg1UdwkajUQjFHsMmke4oYDhpfRbePGOkAMLPBnj4JU1WV', '127.0.0.1', '2018-07-31 16:39:01', NULL, '2018-07-31 16:39:01', '2018-07-31 16:39:01', 'System', '2018-07-31 16:39:01', NULL, NULL, NULL),
	(10, 1, '6ZwnoRRZAKtu36rE8Sk0fRfpFuS5VxXVhi1ib8x0QQo4o8mMMt', '127.0.0.1', '2018-07-31 16:45:11', NULL, '2018-07-31 16:45:11', '2018-07-31 16:45:12', 'System', '2018-07-31 16:45:12', NULL, NULL, NULL),
	(11, 1, 'ANAboe7Jngn9EudhWV5P3PLg5xTJMl7knxOUNCtgbH1esql3e6', '127.0.0.1', '2018-07-31 16:46:20', NULL, '2018-07-31 16:46:20', '2018-07-31 16:46:21', 'System', '2018-07-31 16:46:21', NULL, NULL, NULL),
	(12, 1, 'Hhrz8lhPBjzrKaY0hjjkHl0uN2WCiahqquzySI4GKUREpeSdlV', '127.0.0.1', '2018-07-31 16:47:19', NULL, '2018-07-31 16:47:19', '2018-07-31 16:47:19', 'System', '2018-07-31 16:47:19', NULL, NULL, NULL),
	(13, 1, 'Xl0jYdMRrOKVU0l1YjtCrB3Lbk1h3uuunBD1k3hry6q9BsC1oW', '127.0.0.1', '2018-08-01 15:01:42', NULL, '2018-08-01 15:29:30', '2018-08-01 15:01:43', 'System', '2018-08-01 15:29:30', 'guest', NULL, NULL),
	(14, 1, 'iYCVzbc59gXWfuVoTXvRBpmaOvSvFYxAwsb2IZvBbFCrQXd8lF', '127.0.0.1', '2018-08-01 15:29:34', NULL, '2018-08-01 15:29:55', '2018-08-01 15:29:34', 'System', '2018-08-01 15:29:55', 'guest', NULL, NULL),
	(15, 1, 'b8WzPPUme96AoNr8UTXRA8HRjxl7BIxqQpz7OmeeO2PP3EOs0O', '127.0.0.1', '2018-08-01 15:30:03', NULL, '2018-08-01 15:43:41', '2018-08-01 15:30:03', 'System', '2018-08-01 15:43:41', 'guest', NULL, NULL),
	(16, 1, 'D06o0jBZzUnVipsMlF31IbmwfoXKza4JTCSeD2yryzJFvfVf4B', '127.0.0.1', '2018-08-01 15:31:24', NULL, '2018-08-01 15:31:24', '2018-08-01 15:31:24', 'System', '2018-08-01 15:31:24', NULL, NULL, NULL),
	(17, 1, '7qmyHLWKuxrpkQwvz6oL82nAY3TXjRza44cLu2Xat9X2wrQNjo', '127.0.0.1', '2018-08-01 15:43:50', NULL, '2018-08-01 15:55:26', '2018-08-01 15:43:50', 'System', '2018-08-01 15:55:27', 'guest', NULL, NULL),
	(18, 1, 'UcXUeGrMdf8iXQMOfVMdBsVn34yYI6axAh2365kpboG6mZHbXi', '127.0.0.1', '2018-08-01 15:55:34', NULL, '2018-08-01 15:59:08', '2018-08-01 15:55:34', 'System', '2018-08-01 15:59:08', 'guest', NULL, NULL),
	(19, 1, 'gXr21Ilk2M6UzdV6PnfuWh5ljfgUepi7wpf0PfPX5411Xcyqs0', '127.0.0.1', '2018-08-01 15:59:10', NULL, '2018-08-01 16:39:30', '2018-08-01 15:59:10', 'System', '2018-08-01 16:39:30', 'guest', NULL, NULL),
	(20, 1, 'N6gX8edm8GbVST1TTb0rCEwMFSc7BTsATchss5ERKlHVrLo058', '127.0.0.1', '2018-08-01 16:39:33', NULL, '2018-08-01 18:18:01', '2018-08-01 16:39:33', 'System', '2018-08-01 18:18:01', 'guest', NULL, NULL),
	(21, 1, '77sBQm4Ye1ObPkJrhnolSe4cF79y7WhlKVrH66Ln5MKnt9YvKg', '127.0.0.1', '2018-08-01 18:18:02', NULL, '2018-08-01 19:39:47', '2018-08-01 18:18:03', 'System', '2018-08-01 19:39:48', 'guest', NULL, NULL),
	(22, 1, '8PEwSdbuBnIaVlz7u7kdK5tg2GRyOLJsmjbfrNo1iEnssF5ioY', '127.0.0.1', '2018-08-01 19:40:09', NULL, '2018-08-01 19:46:16', '2018-08-01 19:40:11', 'System', '2018-08-01 19:46:16', 'guest', NULL, NULL),
	(23, 1, 'vC0k3guhtAPLm4wf75pvlJa6BmWfj6iJQ4d4TDefvqkbklD3Yg', '127.0.0.1', '2018-08-18 01:57:01', NULL, '2018-08-18 01:59:23', '2018-08-18 01:57:05', 'System', '2018-08-18 01:59:23', 'guest', NULL, NULL),
	(24, 1, '9EZfIyLaTeeRJtoQQ2yUcdpmeENVUuxS8oizuTDOBBi9fJbqaC', '127.0.0.1', '2018-08-18 18:10:28', NULL, '2018-08-18 18:10:30', '2018-08-18 18:10:29', 'System', '2018-08-18 18:10:30', 'guest', NULL, NULL),
	(25, 1, 'Bgo06Zw9yxtyW8FHp5Be9MvYCTTQGyK0RGl7OT3hXFbplZNFog', '127.0.0.1', '2018-09-08 16:21:55', NULL, '2018-09-08 16:28:27', '2018-09-08 16:21:56', 'System', '2018-09-08 16:28:27', 'guest', NULL, NULL),
	(26, 1, 'DAdGZVlaR1Nm9kmpQ7IZs6rCZtGAeladDx1BPWryZbl9AuXaBa', '127.0.0.1', '2018-09-08 16:28:30', NULL, '2018-09-08 16:28:30', '2018-09-08 16:28:30', 'System', '2018-09-08 16:28:30', NULL, NULL, NULL),
	(27, 1, 'PIcdvaGdVBXn81lEaZuGmNdxqxv9UFV5eIh0UEnQ9QpMU7aps5', '127.0.0.1', '2018-09-08 17:37:52', NULL, '2018-09-08 18:26:48', '2018-09-08 17:37:52', 'System', '2018-09-08 18:26:49', 'guest', NULL, NULL),
	(28, 1, 'eezktu4POaxuGzj4bMLTKMa7olpmH6iDvOWXtN7NgXL0EMwr3q', '127.0.0.1', '2018-09-08 18:26:50', NULL, '2018-09-08 18:28:14', '2018-09-08 18:26:50', 'System', '2018-09-08 18:28:14', 'guest', NULL, NULL),
	(29, 1, 'HvdchackSq1aaYgSU1EUkpibXHomy6BsXkrBrO14bFBql2SWAB', '127.0.0.1', '2018-09-08 20:07:23', NULL, '2018-09-08 20:10:45', '2018-09-08 20:07:24', 'System', '2018-09-08 20:10:45', 'guest', NULL, NULL),
	(30, 1, 'R5xqBE4X6KIcyQ38hqMXFsK9ptAvnXoCboMnOUqjWpKSwJZdTV', '127.0.0.1', '2018-09-09 05:33:33', NULL, '2018-09-09 06:10:13', '2018-09-09 05:33:33', 'System', '2018-09-09 06:10:13', 'guest', NULL, NULL);
/*!40000 ALTER TABLE `user_session` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
